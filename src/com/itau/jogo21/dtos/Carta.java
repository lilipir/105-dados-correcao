package com.itau.jogo21.dtos;

import com.itau.jogo21.enums.Naipe;
import com.itau.jogo21.enums.Rank;

public class Carta {
  private Naipe naipe;
  private Rank rank;
  
  public Carta(Naipe naipe, Rank rank) {
    this.naipe = naipe;
    this.rank = rank;
  }
  
  public Naipe getNaipe() {
    return naipe;
  }
  
  public Rank getRank() {
    return rank;
  }
  
  @Override
  public String toString() {
    return rank + " de " + naipe;
  }
}
